# -*- coding: utf-8 -*-

import sys, os, re

def main(argv = sys.argv):
    path = sys.argv[1]
    file_lists = os.walk(path)
    with open('datas.txt', 'a') as datafile:
        for root, _, files in file_lists:
            for file in files:
                tmp = os.popen('hg log -d "may 2010 to mar 2012" ' + os.path.join(root, file)).readlines()
                if len(tmp) == 0:
                    continue
                churn = 0
                authors = []
                for line in tmp:
                    if len(line) > 9 and line[:9] == 'changeset':
                        churn += 1
                    elif len(line) > 5 and line[:4] == 'user':
                        pos = re.search('<', line)
                        if pos is not None:
                            authors += [line[5:pos.span()[1]].strip()]
                unique = len(set(authors))
                datafile.write('0 ' + str(churn) + ' ' + str(unique) + ' 0\n')
                # print("churn =", churn, "unique =", unique)

if __name__ == "__main__":
    sys.exit(main())
