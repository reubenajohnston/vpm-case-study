% Build partial dataset based on cleaned_dataset.csv
data = csvread('cleaned_dataset.csv', 1, 1);
data = data(:, 1:4);
N = size(data, 1);
data = data(randperm(N),:);

train_num = floor(N * 0.8);
tr_data = data(1:train_num, :);
ts_data = data(train_num+1:end, :);

save('partial_data', 'data', 'tr_data', 'ts_data');

% Build full dataset
load('full_dataset.mat');
N = size(data, 1);
data = data(randperm(N),:);
train_num = floor(N * 0.8);
tr_data = data(1:train_num, :);
ts_data = data(train_num+1:end, :);

save('full_data', 'data', 'tr_data', 'ts_data');