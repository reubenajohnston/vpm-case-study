function model = RandomForest(data)

model = TreeBagger(10, data(:, 1:end-1), data(:, end), ...
    'Method', 'classification');